# Blogsite

This is a simple yet elegant blog website created using JAMSTACK.

## Getting started

- Clone the repo
- In the terminal run : 
    - ``` npm i ```
    - ``` npm build ```
    - ``` npm start ```
- Now open this link [http://localhost:8080](http://localhost:8080) in the browser. You are good to see the blog posts
