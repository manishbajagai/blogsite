module.exports = (eleventyConfig) => {
  eleventyConfig.addShortcode("excerpt", (article) => extractExcerpt(article));

  eleventyConfig.addPassthroughCopy("css");
};

function extractExcerpt(article) {
    
    let excerpt = null;
    const content = article.templateContent;
    
    if (!article.hasOwnProperty("templateContent")) {
      return null;
    }

  const separatorsList = [
    { start: "<!-- Excerpt Start -->", end: "<!-- Excerpt End -->" },
    { start: "<p>", end: "</p>" },
  ];

  separatorsList.some((separators) => {
    const startPosition = content.indexOf(separators.start);

    if (startPosition !== -1) {
      excerpt = content
        .substring(startPosition + separators.start.length, startPosition + separators.start.length+30)
        .trim();
      return true;
    }
  });

  return excerpt;
}
