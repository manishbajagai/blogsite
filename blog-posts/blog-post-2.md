---
layout: post-layout.njk 
title: Second post including images and others
date: 2022-05-11
tags: ['post']
---

# _Ada Lovelace and the first computer programme in the world_

This is the biography of Ada Lovelace. Following is the content for the same

### _Contents_ :

1. Biography
   - Photo
2. About her
3. Facts
4. External Links or References

<hr/>

### **Biography** :

[Augusta Ada Byron](https://en.wikipedia.org/wiki/Ada_Lovelace) was born on the 10th December 1815,as the daughter of Anne Isabella Noel-Byron (known as Annabella) and famous English poet Lord Byron.

<center>

![The Countess of Lovelace](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Ada_Byron_daguerreotype_by_Antoine_Claudet_1843_or_1850.jpg/220px-Ada_Byron_daguerreotype_by_Antoine_Claudet_1843_or_1850.jpg)

</center>

### **About her** :

12-year-old Ada, who is especially interested in mechanics, wants to invent a flying machine - unfortunately, with no success. But the love for machines is the foundation of Ada Lovelace's later friendship with mathematician [Charles Babbage](https://en.wikipedia.org/wiki/Charles_Babbage), whom she meets at a reception when she is 17 years old.

Babbage is working on a prototype of a calculating machine called "The Difference Engine", and Ada is intrigued by his ideas. Ada and Babbage strike up a correspondence over mathematical ideas that lasts for many years.

<hr/>

### **Facts** :

> About a century before [Konrad Zuse](https://en.wikipedia.org/wiki/Konrad_Zuse) designed the first programmable computing machine, in the 1840s. Ada Lovelace wrote the first computer programme in the world. From a modern perspective, her work is visionary.

<hr/>

### **References** :

- [\"Ada\'s Army gets set to rewrite history at Inspirefest 2018\"](https://www.siliconrepublic.com/video/adas-army-zoe-philpott) by Luke Maxwell, 4 August 2018
- [Works by Ada Lovelace](https://openlibrary.org/authors/OL776398A) at Open Library
- [\"Ada Lovelace: Founder of Scientific Computing\"](https://web.archive.org/web/20181225024327/https://www.sdsc.edu/ScienceWomen/lovelace.html%20). Women in Science. SDSC. Archived from the original on 25 December 2018. Retrieved 17 August 2001.
- [\"The fascinating story Ada Lovelace\"](https://www.youtube.com/watch?v=hRmEYMiphoU). Sabine Allaeys – via [YouTube](https://en.wikipedia.org/wiki/YouTube).
- [\"How Ada Lovelace, Lord Byron\'s Daughter, Became the World\'s First Computer Programmer\"](http://www.brainpickings.org/2014/12/10/ada-lovelace-walter-isaacson-innovators/). Maria Popova (Brain). 10 December 2014.

### Testing out code :
```
print('Hello World')

```